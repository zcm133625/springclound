package org.example.client;

import org.example.bean.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component

public class ProductClientRibbon {

    @Autowired
    RestTemplate restTemplate;

    @LoadBalanced

    public List<Product> listProdcuts() {
        return restTemplate.getForObject("http://child2/products",List.class);
    }

}
